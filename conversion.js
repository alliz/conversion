class Token {
  constructor() {
    this.value = [];
    this.danger = false;
  }
}

const parse = (str) => {
  const len = str.length;
  let token = new Token();
  const container = [];

  for (let i = 0; i < len; i++) {
    const charCode = str.charCodeAt(i);

    // valid token number;
    if (charCode >= 48 && charCode <= 57) {

      if (token.onDanger) {
        if (token.value.length !== 0 || i === 1) {
          throw new Error('In general, Number should not follow space');
        } else {
          token.onDanger = false;
        }
      }
      token.value.push(charCode);
    }

    // is a space;
    if (charCode === 32) {
      if (token.onDanger) {
        throw new Error('Consecutive spaces is forbidden');
      }
      token.onDanger = true;
    }

    // is a dot
    if (charCode === 46) {
      container.unshift(token);
      token = new Token();
    }
  }

  container.unshift(token);
  return container;
};

const reassemble = (tokens) => {
  return tokens.reduce((sum, token, index)=> {
    const { value } = token;
    const data = String.fromCharCode.apply(null, value);

    return sum + data * 2 ** (8 * index);
  }, 0);
}

export default (str) => {
  const tokens = parse(str);
  return reassemble(tokens);
};


