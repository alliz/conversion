import conversion from '../conversion';

test('Coversion works', () => {
  const data = '172.168.5.1';
  const result = conversion(data);
  expect(result).toBe(2896692481);
})

test('With heading zero should also work', () => {
  const data = '172.168.05.01';
  const result = conversion(data);
  expect(result).toBe(2896692481);
})

test('Change input to test robust', () => {
  const data = '12.18.5.1';
  const result = conversion(data);
  expect(result).toBe(202507521);
})

test('Heading space is not permitted', () => {
  expect(() => {
    const data = ' 172.168.5.1';
    const result = conversion(data);
  }).toThrow('In general, Number should not follow space');
})

test('Dot could be precedeing by one space', () => {
  const data = '172 .168.5.1';
  const result = conversion(data);
  expect(result).toBe(2896692481);
});

test('Dot could be followed by one space', () => {
  const data = '172. 168.5.1';
  const result = conversion(data);
});

test('Consecutive spaces should cause an error', () => {
  expect(() => {
    const data = '172.  168.5.1';
    const result = conversion(data);
  }).toThrow('Consecutive spaces is forbidden');
});
